import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <legend>{{title}}</legend>
    <home></home>
  `,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Angular 2, vamos aprender!';
}
