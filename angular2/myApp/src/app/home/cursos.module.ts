import { NgModule }      from '@angular/core';
import { CommonModule } from '@angular/common';

import { CursosService } from './cursos.service';
import { HomeComponent } from './home.component';

@NgModule({
    imports: [ CommonModule ],
    declarations: [ HomeComponent ],
    exports: [ HomeComponent ],
    providers: [CursosService]
})
export class CursosModule { }
