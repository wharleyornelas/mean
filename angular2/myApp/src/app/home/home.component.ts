import { Component } from '@angular/core';

import { CursosService } from './cursos.service';

@Component({
  selector: 'home',
  template: `
    <h3>Cursos {{ nomePortal }}</h3>
    <ul>
      <li *ngFor="let curso of cursos">
        {{ curso }}
      </li>
    </ul>
  `
})
export class HomeComponent {
  nomePortal = "Estudos works";

  cursos;

  constructor(cursosService: CursosService) {
    this.cursos = cursosService.getCursos();
  }

}
